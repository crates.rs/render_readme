use std::borrow::Cow;
use std::time::Instant;

// TODO: add a deadline. timeout is not enough for pages with lots of images.

pub struct FilteredImage<'a> {
    pub src: Cow<'a, str>,
    pub srcset: Option<Cow<'a, str>>,
    pub width: Option<u32>,
    pub height: Option<u32>,
}

/// Callbacks for every image URL in the document
pub trait ImageFilter: Send + Sync + 'static {
    /// Ability to change the image URL
    /// Returns 1x image and 2x image, and width/height
    fn filter_url<'a>(&self, url: &'a str, wanted_width: Option<u32>, wanted_height: Option<u32>, _container_width: u32, deadline: Instant) -> FilteredImage<'a>;
}

impl ImageFilter for () {
    fn filter_url<'a>(&self, url: &'a str, width: Option<u32>, height: Option<u32>, _container_width: u32, _deadline: Instant) -> FilteredImage<'a> {
        FilteredImage {
            src: url.into(),
            srcset: None,
            width, height,
        }
    }
}
